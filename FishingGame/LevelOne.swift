//
//  LevelOne.swift
//  FishingGame
//
//  Created by santosh tekulapally on 2019-10-19.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import Foundation
import SpriteKit



class LevelOne:SKScene, SKPhysicsContactDelegate {
    
    
    var waterwall:SKSpriteNode!
    var leftborder:SKSpriteNode!
    var rightborder:SKSpriteNode!
    var goodfish:SKSpriteNode!
    var badfish:SKSpriteNode!
    var tortoise:SKSpriteNode!
    var octopus:SKSpriteNode!
    var anchor:SKSpriteNode!
    
   // let cameraNode = SKCameraNode()
    var fish: SKSpriteNode?

    var Supergoodfish:SKSpriteNode!
  
    
    
    var fishflying = false

    var gamerunning = true
    
    var livesLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    
    var lives = 5
    var score = 0
   
 var catPosition = "left"


    var mouseX:CGFloat = 0
    var mouseY:CGFloat = 0
    var  datestring:String!
    var timenow = 0
  
    let now = Date()
    
    let formatter = DateFormatter()
    
     func checkanchorposition(){
        
         print("anchor is  = \(anchor.position.y)")
        
        if(anchor.position.y > 615)
        {
            
            print("anchor moved away from screen")
            gamerunning = false
            
            showAlert(withTitle:"Phewww", message: "Happy Fishing")
        }
    }
    override func didMove(to view: SKView)
    {
       
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = "HH"
        
        datestring = formatter.string(from: now)

        let optionalString = datestring
        
        // now unwrap it
        if let unwrapped = optionalString {
            print(unwrapped)
            
            
            self.timenow = Int(unwrapped)!
            print(timenow)

        }
        
        
        self.physicsWorld.contactDelegate = self

      
//        addChild(cameraNode)
//        camera = cameraNode
//        cameraNode.position = CGPoint(x: size.width/2, y: size.height/2)
        
        
        self.livesLabel = SKLabelNode(text: "Lives Remaining: \(lives)")
        self.livesLabel.position = CGPoint(x:150, y:500)
        self.livesLabel.fontColor = UIColor.magenta
        self.livesLabel.fontSize = 30
        self.livesLabel.fontName = "Avenir"
        addChild(self.livesLabel)
        
        
        
        
        // Add a score label
        self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
        self.scoreLabel.position = CGPoint(x:160, y:540)
        self.scoreLabel.fontColor = UIColor.magenta
        self.scoreLabel.fontSize = 30
        self.scoreLabel.fontName = "Avenir"
        addChild(self.scoreLabel)
        
    
        self.anchor = SKSpriteNode(imageNamed: "big-anchor")
        let presetTexture = SKTexture(imageNamed: "big-anchor")
        anchor = SKSpriteNode(texture: presetTexture)
        anchor.physicsBody = SKPhysicsBody(texture: presetTexture, size: presetTexture.size())
        self.anchor.zPosition = 5

        anchor.physicsBody?.isDynamic = false
        anchor.physicsBody?.allowsRotation = false
      //  anchor.physicsBody?.affectedByGravity = true
//        anchor.physicsBody!.velocity.dy = 0
     //   physicsWorld.gravity = CGVector(dx: 0, dy: -1.62)

        self.anchor.position = CGPoint(x: -177.171, y:385.60)

        addChild(anchor)
        
        checkanchorposition()


        
        
        let m2 = SKAction.moveBy(x: -500, y: 0, duration: 30)
        
        // 2. put actions into a sequence
        let sequence = SKAction.sequence([m2])
        

        
        //move left to right
        let m1 = SKAction.moveBy(x: 700, y: 0, duration: 30)
        
        // 2. put actions into a sequence
        let sequenceone = SKAction.sequence([m1])
        
    

    }
    var cats:[SKSpriteNode] = []
    
    func spawnCat() {
       let randomgenerator = Int.random(in: 1...18)
        if randomgenerator == 1
        {
            
            fish = SKSpriteNode(imageNamed: "octopus-1")
        }
        else if randomgenerator == 2
        {
            fish = SKSpriteNode(imageNamed: "turtle")
        }
         else if randomgenerator == 3
        {
            fish = SKSpriteNode(imageNamed: "fish2")
        }
        else if randomgenerator == 4
        {
            fish = SKSpriteNode(imageNamed: "style2_1")
        }
        else if randomgenerator == 5
        {
            fish = SKSpriteNode(imageNamed: "style2_16")
        }
        else if randomgenerator == 6
        {
            fish = SKSpriteNode(imageNamed: "style2_3")
        }
        else if randomgenerator == 7
        {
            fish = SKSpriteNode(imageNamed: "style2_4")
        }
        else if randomgenerator == 8
        {
            fish = SKSpriteNode(imageNamed: "style2_5")
        }
        else if randomgenerator == 9
        {
            fish = SKSpriteNode(imageNamed: "style2_6")
        }
        else if randomgenerator == 10
        {
            fish = SKSpriteNode(imageNamed: "style2_7")
        }
        else if randomgenerator == 11
        {
            fish = SKSpriteNode(imageNamed: "style2_8")
        }
        else if randomgenerator == 12
        {
            fish = SKSpriteNode(imageNamed: "style2_9")
        }
        else if randomgenerator == 13
        {
            fish = SKSpriteNode(imageNamed: "style2_10")
        }
        else if randomgenerator == 14
        {
            fish = SKSpriteNode(imageNamed: "style2_11")
        }
        else if randomgenerator == 15
        {
            fish = SKSpriteNode(imageNamed: "style2_12")
        }
        else if randomgenerator == 16
        {
            fish = SKSpriteNode(imageNamed: "style2_13")
        }
        else if randomgenerator == 17
        {
            fish = SKSpriteNode(imageNamed: "style2_14")
        }
        else if randomgenerator == 18
        {
            if(timenow > 10)
            {
                fish = SKSpriteNode(imageNamed: "style2_15")

            }
            else{
                fish = SKSpriteNode(imageNamed: "style2_15")

            }
        }
        // Add a fish to a static location
       
       
       
        
        
        
        // generate a random x position
        let randomXPos =  CGFloat(-290)
       // let randomXPos = CGFloat.random(in: -350 ... -50)
       let randomYPos = CGFloat.random(in:  -645 ... 390)
       
        
        
        //new added readme
        print("random x positio n is  = \(randomXPos)")
        print("random y positio n is = \(randomYPos)")
        
        fish?.size = CGSize(width: 100, height: 60)
        //new
        
        


       fish?.position = CGPoint(x:randomXPos, y:randomYPos)

        let kkk = SKAction.moveBy(x: 1000, y:0, duration: 30)
        let kk = SKAction.moveBy(x: 0, y: 0, duration: 30)
        
        let sequenll = SKAction.sequence([kkk,kk])
        fish?.run(sequenll)
        
        self.fish?.name = "fishey";
       fish?.isUserInteractionEnabled = false
        // add the fish to the screen
        addChild(fish!)
        

        // add the fish to the array
        self.cats.append(fish!)
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
      //  if (gamerunning == false) {
            
         //   anchor?.isUserInteractionEnabled = false

            
//            let touch = touches.first
//            let location = touch?.location(in: self)
//
//            for touch in touches {
//                anchor?.position.x = (location?.x)!
//                anchor?.position.y = (location?.y)!
//            }
        
            
       // }
     //   else{
         //   print("no interration boy")
     //   }
       
        
        
        
    
    
                
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            // if the  touch results in a null value, then exit
            return
        }
        if (fishflying == true) {
            
        fish?.isUserInteractionEnabled = true
        
        // get the sprite that was touched
        let positionInScene = touch.location(in: self)
        var touchedNode = self.atPoint(positionInScene)
        
        // check the name of the sprite that was touched
        if let name = touchedNode.name {
            if (name == "fishey") {
                
                
                self.score = self.score + 1
                self.scoreLabel.text = "Score: \(score)"
                
//                anchor.physicsBody?.isDynamic = false
//
//                anchor.physicsBody?.affectedByGravity = false
//                anchor.physicsBody?.allowsRotation = true

             
                
                touchedNode = touchedNode as! SKSpriteNode
                
               // showAlert(withTitle:"Phewww", message: "HAppy Fishing")
                
                
              //  self.view!.isUserInteractionEnabled = false
                
            }
        }
            
        }
        
        else{
            
            
            print("actually game is running ")
        }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // 1. @TODO: write the code to get the mouse location
        let locationTouched = touches.first
        if (locationTouched == nil) {
            // an error occured
            return
        }
        
        let mousePosition = locationTouched!.location(in:self)
        
        print("mouseX = \(mousePosition.x)")
        print("mouseY = \(mousePosition.y)")
        
        
//        print("-------")
        
        
        // 2. @TODO: set the global mosueX and mouseY variables to the mouse (x,y)
        self.mouseX = mousePosition.x
       self.mouseY = mousePosition.y
        
        
    }
    
    func showAlert(withTitle title: String, message: String) {
        
        
        let xNSNumber = score as NSNumber
        let xString : String = xNSNumber.stringValue
        
        let alert = UIAlertController(title: "Game Over!! Your score is ", message: xString, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//        alert.addTextField(configurationHandler: { textField in
//            textField.placeholder = "Input your name here..."
//        })
//        guard let startScene = SKScene(fileNamed: "GameScene") else { return } // fileNamed is the name you gave the .sks file
//        self.view?.presentScene(startScene)
        
        //            if let scene = SKScene(fileNamed: "Gamescene") {
        //
        //
        //                scene.scaleMode = .aspectFill
        //                // OPTION 1: Change screens with an animation
        //                self.view?.presentScene(scene, transition: SKTransition.flipHorizontal(withDuration: 2.5))
        //                // OPTION 2: Change screens with no animation
        //                //self.view?.presentScene(scene)
        //            }
        // OPTION 1: Change screens with an animation
        
        
    
        
        alert.addAction(UIAlertAction(title: "Restart game", style: .default, handler: { action in
            
            self.anchor.removeFromParent()
            self.repositionanchor()
            self.gamerunning = true
            self.fishflying = false
            self.score = 0
            self.scoreLabel.text = "Score: \(self.score)"
//            guard let startScene = SKScene(fileNamed: "GameScene") else { return } // fileNamed is the name you gave the .sks file
//                        self.view?.presentScene(startScene)

//            if let scene = SKScene(fileNamed: "Gamescene") {
//
//
//                scene.scaleMode = .aspectFill
//                // OPTION 1: Change screens with an animation
//                self.view?.presentScene(scene, transition: SKTransition.flipHorizontal(withDuration: 2.5))
//                // OPTION 2: Change screens with no animation
//                //self.view?.presentScene(scene)
//            }
            // OPTION 1: Change screens with an animation
            
            
        }))
       // self.repositionanchor()
        //self.repositionanchor()

       //] self.gamerunning = true

        view?.window?.rootViewController?.present(alert, animated: true)

       
        
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//
//        let okAction = UIAlertAction(title: "OK", style: .cancel) { _ in }
//        alertController.addAction(okAction)
//
//
//
//
//        view?.window?.rootViewController?.present(alertController, animated: true)
    }
    
    
    
//    func didBegin(_ contact: SKPhysicsContact) {
//        let nodeA = contact.bodyA.node
//        let nodeB = contact.bodyB.node
//
//        if (nodeA == nil || nodeB == nil) {
//            return
//        }
//
//        if (nodeA!.name == "anchor" && nodeB!.name == "goodone") {
//            // player die
//            print("RESETTING POSITION-AAAA")
//
//        }
//    }
    
    func moveAnchor(mouseXPosition:CGFloat, mouseYPostion:CGFloat) {
        
        // move the zombie towards the mouse
        // @TODO: Get the android code for moving bullet towards enemey
        // Implement the algorithm in Swift
        
        // 1. calculate disatnce between mouse and zombie
        let a = (self.mouseX - self.anchor.position.x);
        let b = (self.mouseY - self.anchor.position.y);
        let distance = sqrt((a * a) + (b * b))
        
        // 2. calculate the "rate" to move
        let xn = (a / distance)
        let yn = (b / distance)
        
        // 3. move the bullet
        self.anchor.position.x = self.anchor.position.x + (xn * 2);
        self.anchor.position.y = self.anchor.position.y + (yn * 2);
        
    }
    
    var numLoops = 0

    override func update(_ currentTime: TimeInterval) {
        
        
        numLoops = numLoops + 1
        if (numLoops % 120 == 0) {
            // make a fish
            self.spawnCat()
            
        }
         if(gamerunning == true){
  
            let playSound = SKAction.playSoundFileNamed("Drop.mp3",waitForCompletion: false)

        let moveup = SKAction.moveBy(x:0, y:500, duration: 120)
        let sequencebck = SKAction.sequence([playSound,moveup])
  
        for(index,fish) in cats.enumerated(){
            
    
             if (self.anchor.intersects(fish) == true) {
                
                anchor.run(SKAction.repeatForever(sequencebck))
                fish.run(SKAction.repeatForever(sequencebck))
                
                
                // 2. put actions into a sequence

                anchor.physicsBody?.isDynamic = false
               anchor.physicsBody?.allowsRotation = true
                anchor.physicsBody?.affectedByGravity = false
//
               fishflying = true

                print("fish interseted ")
    
            }
            
             else{
                
                gamerunning = true

            }
        }
    
        
            checkanchorposition()

        }
        else{
            gamerunning == false
        }
        
        
       self.moveAnchor(mouseXPosition: self.mouseX, mouseYPostion: self.mouseY)
    }
    func repositionanchor() {
        
        self.anchor = SKSpriteNode(imageNamed: "big-anchor")
        let presetTexture = SKTexture(imageNamed: "big-anchor")
        anchor = SKSpriteNode(texture: presetTexture)
        anchor.physicsBody = SKPhysicsBody(texture: presetTexture, size: presetTexture.size())
        self.anchor.zPosition = 5
        
        anchor.physicsBody?.isDynamic = false
        anchor.physicsBody?.allowsRotation = false
        //  anchor.physicsBody?.affectedByGravity = true
        //        anchor.physicsBody!.velocity.dy = 0
        //   physicsWorld.gravity = CGVector(dx: 0, dy: -1.62)
        
        self.anchor.position = CGPoint(x: -177.171, y:385.60)
        
        addChild(anchor)
        
    }
}
