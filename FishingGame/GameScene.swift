//
//  GameScene.swift
//  FishingGame
//
//  Created by santosh tekulapally on 2019-10-15.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    var anchor:SKNode?
    var Fisherman:SKNode?
       var clod1:SKNode?
    var clod2:SKNode?
    var clod3:SKNode?
    
    var waterline:SKNode?

   
    var livesLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    
    var lives = 5
    var score = 0
    
   // var mouseX:CGFloat = 0
  //  var mouseY:CGFloat = 0
    
    var counter = 0
  
//new//
 
    let  waveMoveLeft = true
    
    var timer = Timer()
    
  
    func scheduledTimerWithTimeInterval(){

        timer = Timer.scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        
     
       
    
}
    
    
    
    
    
    
    override func didMove(to view: SKView) {
        
  
        
        scheduledTimerWithTimeInterval()

  
    
        self.waterline = self.childNode(withName: "waterwave")

    
        self.anchor = self.childNode(withName: "anchor")

        self.Fisherman = self.childNode(withName: "fisherman")
        self.clod1 = self.childNode(withName: "cloud1")
        self.clod2 = self.childNode(withName: "cloud2")
        self.clod3 = self.childNode(withName: "cloud3")

        
        self.livesLabel = SKLabelNode(text: "Lives Remaining: \(lives)")
      self.livesLabel.position = CGPoint(x:150, y:200)
        self.livesLabel.zPosition = 7

        self.livesLabel.fontColor = UIColor.magenta
        self.livesLabel.fontSize = 30
        self.livesLabel.fontName = "Avenir"
        addChild(self.livesLabel)
        
       // self.playsound()
        
        
        // Add a score label
        self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
        self.scoreLabel.position = CGPoint(x:160, y:240)
        self.scoreLabel.fontColor = UIColor.magenta
        self.scoreLabel.zPosition = 7

        self.scoreLabel.fontSize = 30
        self.scoreLabel.fontName = "Avenir"
        addChild(self.scoreLabel)
      //  self.poo = self.childNode(withName:"poop")
        
       // self.cloud1 = SKSpriteNode(imageNamed: "CloudOne")

       // self.cloud1 = self.childNode(withName:"cloud1")

      //  self.fisherman = self.childNode(withName:"fisherman")
//
//        let m1 = SKAction.moveBy(x: -70, y: 0, duration: 5)
//        // --- 1b. move right
//        let m2 = SKAction.moveBy(x: 100, y: 0, duration: 5)
       
        
//        let move1 = SKAction.move(to: CGPoint(x:-70, y: 0),
//                                  duration:50)
//        let move2 = SKAction.move(to:CGPoint(x:100, y:0), duration:50)
//
      //   let c1 = SKAction.moveBy(x: 200, y: 0, duration: 5)
        
      //  let c2 = SKAction.moveBy(x: 100, y: 0, duration: 5)

        
      //  let sequenceone = SKAction.sequence([move1,move2])
//
    //  self.cloud1!.run(SKAction.repeatForever(sequenceone))
    
        
        let cloudmove1 = SKAction.moveBy(x: 700, y: 0, duration: 10)
        // --- 1b. move right
        let cloudmove2 = SKAction.moveBy(x: 700, y: 0, duration: 10)
        
        let cloudmove3 = SKAction.moveBy(x: 700, y: 0, duration: 10)

        // 2. put actions into a sequence
       

      
        
        let m2 = SKAction.moveBy(x: -150, y: 0, duration: 10)
        // --- 1b. move right
        let m1 = SKAction.moveBy(x: 150, y: 0, duration: 10)
        
        let playSound = SKAction.playSoundFileNamed("backgroundMusic.mp3",waitForCompletion: false)

        // 2. put actions into a sequence
        let sequence = SKAction.sequence([playSound,m1,m2])
        
     
        
        // group actions in a sequence
        
        
        // 3. apply sequence to sprite
        self.Fisherman!.run(SKAction.repeatForever(sequence))
        self.anchor!.run(SKAction.repeatForever(sequence))

        self.clod1!.run(cloudmove1)
        self.clod2!.run(cloudmove2)
        self.clod3!.run(cloudmove3)

        // 2. put actions into a sequence
    //    let sequence1 = SKAction.sequence([m2,m1])
      // self.fisherman!.run(SKAction.repeatForever(sequence1))

       
//        print("mouseX = \(self.clod1?.position.x)")
//        print("mouseY = \(self.clod2?.position.x)")
//        print("mouseY = \(self.clod3?.position.x)")
//        print("-------")
        
       

        // Get label node from scene and store it for use later
        self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
        if let label = self.label {
            label.alpha = 0.0
            label.run(SKAction.fadeIn(withDuration: 2.0))
        }
        
       
    }
    var clouds:[SKSpriteNode] = []

    func spawnCloud() {
        // Add a cat to a static location
        let cloud = SKSpriteNode(imageNamed: "cloud1")
        
        // generate a random x position
        
        let randomXPos = CGFloat.random(in: 0 ... size.width)
        let randomYPos = CGFloat.random(in: 0 ... size.height)
        cloud.position = CGPoint(x:randomXPos, y:0)
        
        // add the cat to the screen
        addChild(cloud)
        
        // add the cat to the array
        self.clouds.append(cloud)
        
    }
  
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
       
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // ---------------------------------------------
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "Start game") {
            if let scene = SKScene(fileNamed: "LevelOne") {
                scene.scaleMode = .aspectFill
                // OPTION 1: Change screens with an animation
                self.view?.presentScene(scene, transition: SKTransition.flipHorizontal(withDuration: 2.5))
                // OPTION 2: Change screens with no animation
                //self.view?.presentScene(scene)
            }
        }
       
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
  //  var numLoops = 0
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
//        numLoops = numLoops + 1
//        if (numLoops % 120 == 0) {
//            // make a cat
//            self.spawnCat()
//        }
    }
    
    
}
